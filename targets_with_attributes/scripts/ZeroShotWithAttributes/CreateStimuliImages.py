import sys
import os
import os.path as path
import argparse
import numpy as np
import torch
import torch.nn as nn
import torchvision
import platform
import random
import torchvision.transforms as transforms
import torchvision.transforms.functional as functional

from PIL import Image
from torch.utils import data
from datetime import datetime
from TargetDataset import TargetDataset

plt = platform.system()

total_stimuli_image_created = 0
single_stimuli_image_created = 0
multiple_stimuli_image_created = 0


class TargetLocation:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def create_images(root_path, dataset_loader, target_image_size, stimuli_image_width, stimuli_image_height, classes):
    global total_stimuli_image_created
    global single_stimuli_image_created
    global multiple_stimuli_image_created

    dataset_length = len(dataset_loader)

    for i, (images, features, img_names, indexes) in enumerate(dataset_loader):
        stimuli_index = random.randint(0, 203)
        image_orig = Image.open(root_path + 'data/Stimuli/V3/img' + str(stimuli_index + 1).zfill(3) + '.png') \
            .convert('RGBA')
        image_postfix = str(stimuli_index + 1).zfill(3)

        image_orig_temp = image_orig.copy()
        black_white_image = np.zeros([1024, 1280], dtype=np.uint8)
        black_white_image = Image.fromarray(black_white_image, '1')

        random_image_size_coef = random.uniform(0.5, 1.0)
        altered_target_image_size = int(target_image_size * random_image_size_coef)

        random_target_image_locations = []
        multiple_images_put = False
        images_put = False

        print(i / dataset_length * 100, " percent complete", end='\r')
        sys.stdout.flush()

        class_paste_loc = dict()

        dup_index = 1
        for image_index in range(len(images)):
            target_image = images[image_index]
            target_image = transforms.ToPILImage()(target_image)
            target_image = functional.resize(target_image, (altered_target_image_size, altered_target_image_size))

            white_target_image = np.zeros([altered_target_image_size, altered_target_image_size], dtype=np.uint8)
            white_target_image.fill(255)
            white_target_image = Image.fromarray(white_target_image, '1')

            random_target_image_pos_x = -1
            random_target_image_pos_y = -1
            base_target_image_pos_x = random.randint(0, stimuli_image_width - altered_target_image_size + 1)
            base_target_image_pos_y = random.randint(0, stimuli_image_height - altered_target_image_size + 1)
            if len(random_target_image_locations) > 0:
                candidate_random_target_image_pos_x = -1
                candidate_random_target_image_pos_y = -1
                max_try = 100
                try_count = 0
                while try_count < max_try and (random_target_image_pos_x == -1 or random_target_image_pos_y == -1):
                    for random_target_location in random_target_image_locations:
                        if base_target_image_pos_x + altered_target_image_size <= random_target_location.x \
                                or base_target_image_pos_x >= (random_target_location.x + altered_target_image_size):
                            candidate_random_target_image_pos_x = base_target_image_pos_x

                        if base_target_image_pos_y + altered_target_image_size <= random_target_location.y \
                                or base_target_image_pos_y >= (random_target_location.y + altered_target_image_size):
                            candidate_random_target_image_pos_y = base_target_image_pos_y

                    if candidate_random_target_image_pos_x >= 0 and candidate_random_target_image_pos_y >= 0:
                        random_target_image_pos_x = candidate_random_target_image_pos_x
                        random_target_image_pos_y = candidate_random_target_image_pos_y
                        break
                    else:
                        base_target_image_pos_x = random.randint(0, stimuli_image_width - altered_target_image_size + 1)
                        base_target_image_pos_y = random.randint(0,
                                                                 stimuli_image_height - altered_target_image_size + 1)

                    try_count = try_count + 1

                if random_target_image_pos_x == -1 or random_target_image_pos_y == -1:
                    print('[WARNING!] Target image is skipped')
                    continue
                else:
                    multiple_images_put = True
            else:
                random_target_image_pos_x = base_target_image_pos_x
                random_target_image_pos_y = base_target_image_pos_y

            # X as horizontal and Y as vertical
            random_target_image_locations.append(TargetLocation(random_target_image_pos_x, random_target_image_pos_y))

            image_orig_temp.paste(target_image, [random_target_image_pos_x, random_target_image_pos_y], target_image)
            black_white_image.paste(white_target_image, [random_target_image_pos_x, random_target_image_pos_y])

            if classes[indexes[image_index]] not in class_paste_loc:
                class_paste_loc[classes[indexes[image_index]]] = [random_target_image_pos_x, random_target_image_pos_y]
            else:
                class_paste_loc[classes[indexes[image_index]] + '#' + str(dup_index)] = \
                    [random_target_image_pos_x, random_target_image_pos_y]
                dup_index = dup_index + 1

            # This may result single ones fewer then multiple ones (Because of randomness of the loader)
            if i < dataset_length / 2:
                if not path.exists(
                        root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/' + classes[indexes[image_index]]
                        .replace('+', ' ')):
                    os.mkdir(
                        root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/' + classes[indexes[image_index]]
                        .replace('+', ' '))
                if not path.exists(
                        root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/' + classes[indexes[image_index]]
                        .replace('+', ' ')):
                    os.mkdir(
                        root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/' + classes[indexes[image_index]]
                        .replace('+', ' '))

                image_orig_temp.convert('RGB').save(
                    root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/'
                    + classes[indexes[image_index]].replace('+', ' ')
                    + '/img' + image_postfix + '_' + str(single_stimuli_image_created + 1).zfill(3) + '_'
                    + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[indexes[image_index]]][0])
                    + '_' + str(class_paste_loc[classes[indexes[image_index]]][1]) + '.jpg')
                black_white_image.save(
                    root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/'
                    + classes[indexes[image_index]].replace('+', ' ')
                    + '/img' + image_postfix + '_' + str(single_stimuli_image_created + 1).zfill(3) + '_'
                    + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[indexes[image_index]]][0])
                    + '_' + str(class_paste_loc[classes[indexes[image_index]]][1]) + '.jpg')

                images_put = True
                image_orig_temp = image_orig.copy()
                black_white_image = np.zeros([1024, 1280], dtype=np.uint8)
                black_white_image = Image.fromarray(black_white_image, '1')
                random_target_image_locations = []

                single_stimuli_image_created = single_stimuli_image_created + 1
                total_stimuli_image_created = total_stimuli_image_created + 1

        if not images_put:
            if multiple_images_put:
                temp_dup_index = 1
                start_dup_index_check = False
                for index in indexes:
                    if not path.exists(
                            root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Multiple/' + classes[index].replace('+',
                                                                                                                    ' ')):
                        os.mkdir(
                            root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Multiple/' + classes[index].replace('+',
                                                                                                                    ' '))
                    if not path.exists(
                            root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Multiple/' + classes[index].replace('+',
                                                                                                                  ' ')):
                        os.mkdir(
                            root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Multiple/' + classes[index].replace('+',
                                                                                                                  ' '))

                    if start_dup_index_check and classes[index] + '#' + str(temp_dup_index) in class_paste_loc:
                        image_orig_temp.convert('RGB').save(
                            root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Multiple/' + classes[index].replace('+',
                                                                                                                    ' ')
                            + '/img' + image_postfix + '_' + str(multiple_stimuli_image_created + 1).zfill(3) + '_'
                            + str(altered_target_image_size) + '_'
                            + str(class_paste_loc[classes[index] + '#' + str(temp_dup_index)][0]) + '_'
                            + str(class_paste_loc[classes[index] + '#' + str(temp_dup_index)][1]) + '.jpg')
                        black_white_image.save(
                            root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Multiple/' + classes[index].replace('+',
                                                                                                                  ' ')
                            + '/img' + image_postfix + '_' + str(multiple_stimuli_image_created + 1).zfill(3) + '_'
                            + str(altered_target_image_size) + '_'
                            + str(class_paste_loc[classes[index] + '#' + str(temp_dup_index)][0]) + '_'
                            + str(class_paste_loc[classes[index] + '#' + str(temp_dup_index)][1]) + '.jpg')
                        temp_dup_index = temp_dup_index + 1
                    else:
                        image_orig_temp.convert('RGB').save(
                            root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Multiple/' + classes[index].replace('+',
                                                                                                                    ' ')
                            + '/img' + image_postfix + '_' + str(multiple_stimuli_image_created + 1).zfill(3) + '_'
                            + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[index]][0]) + '_'
                            + str(class_paste_loc[classes[index]][1]) + '.jpg')
                        black_white_image.save(
                            root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Multiple/' + classes[index].replace('+',
                                                                                                                  ' ')
                            + '/img' + image_postfix + '_' + str(multiple_stimuli_image_created + 1).zfill(3) + '_'
                            + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[index]][0]) + '_'
                            + str(class_paste_loc[classes[index]][1]) + '.jpg')
                    start_dup_index_check = True

                multiple_stimuli_image_created = multiple_stimuli_image_created + len(indexes)
                total_stimuli_image_created = total_stimuli_image_created + len(indexes)
            else:
                if not path.exists(
                        root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/' + classes[indexes[0]].replace('+',
                                                                                                                   ' ')):
                    os.mkdir(
                        root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/' + classes[indexes[0]].replace('+',
                                                                                                                   ' '))
                if not path.exists(
                        root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/' + classes[indexes[0]].replace('+',
                                                                                                                 ' ')):
                    os.mkdir(
                        root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/' + classes[indexes[0]].replace('+',
                                                                                                                 ' '))

                image_orig_temp.convert('RGB').save(
                    root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Single/' + classes[indexes[0]].replace('+', ' ')
                    + '/img' + image_postfix + '_' + str(single_stimuli_image_created + 1).zfill(3) + '_'
                    + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[indexes[0]]][0]) + '_'
                    + str(class_paste_loc[classes[indexes[0]]][1]) + '.jpg')
                black_white_image.save(
                    root_path + 'data/Stimuli/V3/GeneratedGroundTruth/Single/' + classes[indexes[0]].replace('+', ' ')
                    + '/img' + image_postfix + '_' + str(single_stimuli_image_created + 1).zfill(3) + '_'
                    + str(altered_target_image_size) + '_' + str(class_paste_loc[classes[indexes[0]]][0]) + '_'
                    + str(class_paste_loc[classes[indexes[0]]][1]) + '.jpg')

                single_stimuli_image_created = single_stimuli_image_created + 1
                total_stimuli_image_created = total_stimuli_image_created + 1

    print("100.00 percent complete - Single Image Count: " + str(single_stimuli_image_created)
          + " - Multiple Image Count: " + str(multiple_stimuli_image_created))

    return total_stimuli_image_created


def main():
    print("[" + str(datetime.now()) + "] Process started")

    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_35_ones/'
    elif plt == 'Linux':
        root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_35_ones/'
    else:
        sys.exit(0)

    parser = argparse.ArgumentParser()
    parser.add_argument('--max_multiple_image_count', '-m', type=int, nargs='?', const=True, default=2)
    args = parser.parse_args()
    args = vars(args)
    max_multiple_image_count = args['max_multiple_image_count']

    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))
    test_classes = np.array(np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str'))

    test_process_steps = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor()
    ])

    train_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, False, False)
    modified_train_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, False, True)
    test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, False, False)
    modified_test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, False, True)

    global total_stimuli_image_created
    stimuli_image_width = 1280
    stimuli_image_height = 1024
    target_image_size = 224

    for multiple_image_count in range(2, max_multiple_image_count + 1):
        params = {'batch_size': multiple_image_count, 'shuffle': True, 'num_workers': 3}
        train_loader = data.DataLoader(train_dataset, **params)
        modified_train_loader = data.DataLoader(modified_train_dataset, **params)
        test_loader = data.DataLoader(test_dataset, **params)
        modified_test_loader = data.DataLoader(modified_test_dataset, **params)

        print("Processing with train dataset")
        create_images(root_path, train_loader, target_image_size, stimuli_image_width,
                      stimuli_image_height, classes)
        print("Processing with modified train dataset")
        create_images(root_path, modified_train_loader, target_image_size, stimuli_image_width,
                      stimuli_image_height, classes)
        print("Processing with test dataset")
        create_images(root_path, test_loader, target_image_size, stimuli_image_width,
                      stimuli_image_height, classes)
        print("Processing with modified test dataset")
        create_images(root_path, modified_test_loader, target_image_size, stimuli_image_width,
                      stimuli_image_height, classes)

    print("[" + str(datetime.now()) + "] Process finished. Created " + str(
        total_stimuli_image_created) + " stimuli images..")
    exit(0)


if __name__ == '__main__':
    main()
