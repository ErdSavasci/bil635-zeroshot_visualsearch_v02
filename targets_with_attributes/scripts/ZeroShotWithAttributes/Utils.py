from scipy import linalg
import scipy as sp
import torch
import torch.nn as nn
import torchvision
import sys

from TargetDataset import *


def make_batches(x, bs):
    if x <= bs:
        return [min(x, bs)]
    else:
        return [bs] + make_batches(x - bs, bs)


def create_new_weights(original_weights, n_channels, model_index):
    if model_index == 1:
        dst = torch.zeros(64, 4, 7, 7)
    elif model_index == 2:
        dst = torch.zeros(64, 4, 3, 3)
    # Repeat original weights up to fill dimension
    start = 0
    for i in make_batches(n_channels, 3):
        # print('dst',start,start+i, ' = src',0,i)
        dst[:, start:start + i, :, :] = original_weights[:, :i, :, :]
        start = start + i
    return dst


def build_model(num_labels, is_pretrained, is_parallel, device, model_index):
    if model_index == 1:
        model = torchvision.models.resnet50(pretrained=is_pretrained).to(device)
    elif model_index == 2:
        model = torchvision.models.vgg16(pretrained=is_pretrained).to(device)

    if is_pretrained:
        for i, param in model.named_parameters():
            param.requires_grad = False
    if is_parallel:
        print('Using DataParallel:')
        model = nn.DataParallel(model)
        if model_index == 1:
            model_features = model.module.fc.in_features
            model.module.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                            nn.Linear(model_features, 1000),
                                            nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                            nn.Linear(1000, num_labels))
        elif model_index == 2:
            model_features = model.classifier[0].out_features
            model.classifier[-1] = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                                 nn.Linear(model_features, 1000),
                                                 nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                                 nn.Linear(1000, num_labels))
    else:
        print('Not using DataParallel:')
        if model_index == 1:
            model_features = model.fc.in_features
            model.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                     nn.Linear(model_features, 1000),
                                     nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                     nn.Linear(1000, num_labels))
        elif model_index == 2:
            model_features = model.classifier[0].out_features
            model.classifier[-1] = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                                 nn.Linear(model_features, 1000),
                                                 nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                                 nn.Linear(1000, num_labels))

    original_weights = model.conv1.weight.clone()
    new_weights = create_new_weights(original_weights, 4, model_index)
    if model_index == 1:
        new_layer = nn.Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
    elif model_index == 2:
        new_layer = nn.Conv2d(4, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)

    new_layer.weight = nn.Parameter(new_weights, False)
    model.conv1 = new_layer

    if device.type == 'cuda':
        model.cuda()

    print(model)

    return model


def load_model(model, model_file, device):
    is_parallel = torch.cuda.device_count() > 1
    if is_parallel:
        model = torch.nn.DataParallel(model)
        dictionary = torch.load(model_file)
        model = model.module
        model.load_state_dict(dictionary)
    else:
        state_dict = torch.load(model_file)
        model.load_state_dict(state_dict)

    model.to(device)

    return model


def labels_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode):
    predictions = []
    for i in range(pred_labels.shape[0]):
        curr_labels = pred_labels[i, :].cpu().detach().numpy()
        best_dist = sys.maxsize
        best_index = -1
        for j in range(predicate_binary_mat.shape[0]):
            class_labels = predicate_binary_mat[j, :]

            if mode == 'train' and classes[j] in t_classes:
                dist = get_euclidean_dist(curr_labels, class_labels)
                if dist < best_dist:
                    best_index = j
                    best_dist = dist
            if mode == 'train2' and classes[j] in t_classes:
                dist = get_hamming_dist(curr_labels, class_labels)
                if dist < best_dist:
                    best_index = j
                    best_dist = dist
            elif mode == 'test' and classes[j] not in t_classes:
                dist = get_euclidean_dist(curr_labels, class_labels)
                if dist < best_dist:
                    best_index = j
                    best_dist = dist
            elif mode == 'test2' and classes[j] not in t_classes:
                dist = get_hamming_dist(curr_labels, class_labels)
                if dist < best_dist:
                    best_index = j
                    best_dist = dist

        predictions.append(classes[best_index])
    return predictions


def make_predictions(root_path, model, data_loader, output_filename, device, predicate_binary_mat, classes,
                     t_classes, mode, is_single, verbose):
    # Toggle flag
    model.eval()

    pred_classes = []
    output_img_names = []
    with torch.no_grad():
        for i, (images, features, img_names, indexes) in enumerate(data_loader):
            images = images.to(device)
            # features = features.to(device).float()
            outputs = model(images)
            sigmoid_outputs = torch.sigmoid(outputs)
            pred_labels = sigmoid_outputs  # > 0.5

            if mode == 'train2' or mode == 'test2':
                temp = pred_labels[:, :]
                temp[temp >= 0.5] = 1
                temp[temp < 0.5] = 0

            curr_pred_classes = labels_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode)
            pred_classes.extend(curr_pred_classes)
            output_img_names.extend(img_names)

            # if i % 1000 == 0:
            if verbose:
                print('Prediction iter: {}'.format(i + 1), end='\r')
                sys.stdout.flush()

        if mode == 'train':
            output_filename = output_filename.replace('.txt', '') + '_train.txt'
        elif mode == 'test' or mode == 'test2':
            output_filename = output_filename.replace('.txt', '') + '_test.txt'

        with open(root_path + 'output/' + output_filename, 'w') as f:
            for i in range(len(pred_classes)):
                if not is_single:
                    output_name = output_img_names[i].replace(root_path + 'data/MyDataset/', '')
                else:
                    output_name = output_img_names[i].replace(root_path + 'data/PNGImages/', '')
                f.write(output_name.replace(' ', '+') + '###' + pred_classes[i] + '\n')

        if verbose:
            print('Predictions are successfully completed')


def evaluate(model, data_loader, device, classes, predicate_binary_mat, t_classes, mode):
    # Toggle flag
    # model.eval()
    # mean_acc = 0.0

    pred_classes = []
    truth_classes = []
    with torch.no_grad():
        for i, (images, features, img_names, indexes) in enumerate(data_loader):
            images = images.to(device)
            # features = features.to(device).float()
            outputs = model(images)
            sigmoid_outputs = torch.sigmoid(outputs)
            pred_labels = sigmoid_outputs  # > 0.5

            if mode == 'train2' or mode == 'test2':
                temp = pred_labels[:, :]
                temp[temp >= 0.5] = 1
                temp[temp < 0.5] = 0

            curr_pred_classes = labels_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode)
            pred_classes.extend(curr_pred_classes)

            curr_truth_classes = []
            for index in indexes:
                curr_truth_classes.append(classes[index])
            truth_classes.extend(curr_truth_classes)

    pred_classes = np.array(pred_classes)
    truth_classes = np.array(truth_classes)
    mean_acc = np.mean(pred_classes == truth_classes)

    return mean_acc


def get_hamming_dist(curr_labels, class_labels):
    return np.sum(curr_labels != class_labels)


def get_cosine_dist(curr_labels, class_labels):
    return np.sum(curr_labels * class_labels) / np.sqrt(np.sum(curr_labels)) / np.sqrt(np.sum(class_labels))


def get_euclidean_dist(curr_labels, class_labels):
    return np.sqrt(np.sum((curr_labels - class_labels) ** 2))


def get_mahalanobis_distance(x=None, data=None, cov=None):
    """Compute the Mahalanobis Distance between each row of x and the data
    x    : vector or matrix of data with, say, p columns.
    data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
    cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
    """
    x_minus_mu = x - np.mean(data)
    if not cov:
        cov = np.cov(data.T)
    inv_covmat = linalg.pinv(cov)
    left_term = np.dot(x_minus_mu, inv_covmat)
    mahal = np.dot(left_term, x_minus_mu.T)
    return mahal


def debug(model_file, mode, root_path, num_labels, output_filename, device, classes, predicate_binary_mat,
          train_classes, model_index):
    # For only testing

    model = build_model(num_labels, False, False, device, model_index)
    model = load_model(model, model_file, device)
    test_params = {'batch_size': 1, 'shuffle': True, 'num_workers': 3}
    process_steps = transforms.Compose([
        # transforms.RandomRotation(15),
        # transforms.RandomHorizontalFlip(),
        # transforms.ColorJitter(brightness=0.3),
        transforms.Resize((224, 224)),  # ImageNet standard
        transforms.ToTensor()
    ])
    test_dataset = TargetDataset(root_path, 'testclasses.txt', process_steps, False, False)
    test_loader = data.DataLoader(test_dataset, **test_params)
    if mode == 'evaluate':
        print(evaluate(model, test_loader, device, classes, predicate_binary_mat, train_classes, 'test'))
    elif mode == 'predict':
        make_predictions(root_path, model, test_loader, output_filename, device, predicate_binary_mat, classes,
                         train_classes, 'test', False, True)
