import sys
import argparse
import numpy as np
import torch
import torch.nn as nn
import torchvision
import platform
import torchvision.transforms as transforms
import os
import time
import copy
import matplotlib.pyplot as pyplot

from glob import glob
from os import path
from PIL import Image, ImageDraw
from datetime import datetime

plt = platform.system()
processed_images = []


def arg2bool(arg):
    if isinstance(arg, bool):
        return arg
    elif arg.lower() in ('yes', 'true', 'y', 't', 1):
        return True
    elif arg.lower() in ('no', 'false', 'n', 'f', 0):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def make_batches(x, bs):
    if x <= bs:
        return [min(x, bs)]
    else:
        return [bs] + make_batches(x - bs, bs)


def create_new_weights(original_weights, n_channels):
    dst = torch.zeros(64, 4, 7, 7)
    # Repeat original weights up to fill dimension
    start = 0
    for i in make_batches(n_channels, 3):
        # print('dst',start,start+i, ' = src',0,i)
        dst[:, start:start + i, :, :] = original_weights[:, :i, :, :]
        start = start + i
    return dst


def build_model(num_labels, is_pretrained, is_parallel, device):
    model = torchvision.models.resnet50(pretrained=is_pretrained).to(device)
    if is_pretrained:
        for i, param in model.named_parameters():
            param.requires_grad = False
    if is_parallel:
        print('Using parallel GPU')
        model = nn.DataParallel(model)
        model_features = model.module.fc.in_features
        model.module.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(model_features, 1000),
                                        nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(1000, num_labels))
    else:
        print('Using single GPU')
        model_features = model.fc.in_features
        model.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(model_features, 1000),
                                 nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(1000, num_labels))

    if is_parallel:
        original_weights = model.module.conv1.weight.clone()
    else:
        original_weights = model.conv1.weight.clone()

    new_weights = create_new_weights(original_weights, 4)
    new_layer = nn.Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
    new_layer.weight = nn.Parameter(new_weights, False)

    if is_parallel:
        model.module.conv1 = new_layer
    else:
        model.conv1 = new_layer

    if device.type == 'cuda':
        model.cuda()

    return model


def load_model(model_param, model_file, device, is_parallel):
    if is_parallel:
        model_param = nn.DataParallel(model_param)
        dictionary = torch.load(model_file)
        model_param = model_param.module
        for key in list(dictionary.keys()):
            if 'module.' not in key:
                dictionary['module.' + key] = dictionary[key]
                del dictionary[key]
        model_param.load_state_dict(dictionary)
    else:
        state_dict = torch.load(model_file)
        model_param.load_state_dict(state_dict)

    model_param.to(device)

    return model_param


def label_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode):
    curr_labels = pred_labels[0, :].cpu().detach().numpy()
    best_dist = sys.maxsize
    best_index = -1
    for j in range(predicate_binary_mat.shape[0]):
        class_labels = predicate_binary_mat[j, :]

        if mode == 'train' and classes[j] in t_classes:
            dist = get_euclidean_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        if mode == 'train2' and classes[j] in t_classes:
            dist = get_hamming_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        elif mode == 'test' and classes[j] not in t_classes:
            dist = get_euclidean_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        elif mode == 'test2' and classes[j] not in t_classes:
            dist = get_hamming_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist

    return best_dist, best_index


def get_hamming_dist(curr_labels, class_labels):
    return np.sum(curr_labels != class_labels)


def get_euclidean_dist(curr_labels, class_labels):
    return np.sqrt(np.sum((curr_labels - class_labels) ** 2))


def process_images(classes, train_classes, predicate_binary_mat, root_path, is_single, device, loaded_model,
                   stimuli_width, stimuli_height, padding_view, version, test_only, precision_mode, process_classes,
                   debug_mode, max_save_target_image_count):
    if version == 1:
        for class_index in range(len(classes)):
            # Target Name
            to_be_found_class_name = classes[class_index]
            image_orig = Image.open(root_path + 'data/Stimuli/V1/img' + str(class_index + 1).zfill(3) + '.png') \
                .convert('RGBA')
            image = np.array(image_orig)

            target_found = False
            target_ind_dists = dict()
            target_views = [448, 420, 392, 364, 336, 308, 280, 252, 224, 196, 168, 140, 112, 84, 56, 28]
            # target_views = [448, 434, 420, 406, 392, 378, 364, 350, 336, 312, 308, 294, 280, 266, 252, 228, 224, 210,
            #                             196, 182, 168, 154, 140, 126, 112, 98, 84, 70, 56, 42, 28, 14]

            for view_index in range(len(target_views)):
                target_view = target_views[view_index]
                padding_view = target_view
                start_x = 0
                end_x = target_view
                start_y = 0
                end_y = target_view

                while True:
                    cropped_image_orig = image[start_x:end_x, start_y:end_y, :]
                    test_process_steps = transforms.Compose([
                        transforms.Resize((224, 224)),
                        transforms.ToTensor()
                    ])
                    cropped_image_orig = Image.fromarray(cropped_image_orig, 'RGBA')
                    cropped_image = test_process_steps(cropped_image_orig)
                    cropped_image = cropped_image.to(device)
                    cropped_image = cropped_image.reshape([1, 4, 224, 224])

                    outputs = loaded_model(cropped_image)
                    sigmoid_outputs = torch.sigmoid(outputs)
                    pred_labels = sigmoid_outputs

                    if 0 <= class_index <= 23:
                        mode = 'train'
                    else:
                        mode = 'test'

                    if mode == 'train2' or mode == 'test2':
                        temp = pred_labels[:, :]
                        temp[temp >= 0.5] = 1
                        temp[temp < 0.5] = 0

                    best_dist, best_index = label_to_class(pred_labels, predicate_binary_mat, classes, train_classes,
                                                           mode)
                    curr_pred_class = classes[best_index]
                    print('[{}/{}] [{}/{}] To Be Found Class Name: {}|Current Target Prediction: {}'
                          .format(class_index + 1,
                                  len(classes),
                                  view_index + 1,
                                  len(target_views),
                                  to_be_found_class_name,
                                  curr_pred_class))

                    if curr_pred_class == to_be_found_class_name:
                        print('Target Found!')
                        target_found = True
                        target_ind_dists[best_dist] = [cropped_image_orig, start_x, end_x, start_y, end_y]
                        print(
                            '[{}/{}] StartX: {} EndX: {} StartY: {} EndY: {}'.format(view_index + 1, len(target_views),
                                                                                     start_x, end_x, start_y, end_y))

                    start_y = start_y + padding_view
                    end_y = start_y + target_view
                    if start_y > stimuli_width:
                        start_y = 0
                        end_y = target_view
                        start_x = start_x + padding_view
                        end_x = start_x + target_view

                    if end_y > stimuli_width:
                        end_y = stimuli_width

                    if end_x > stimuli_height:
                        end_x = stimuli_height

                    if start_x > stimuli_height:
                        break

                    if end_y - start_y > (end_x - start_x) * 1.25:
                        break

                    if end_x - start_x > (end_y - start_y) * 1.25:
                        start_y = 0
                        end_y = target_view
                        start_x = start_x + padding_view
                        end_x = start_x + target_view

            if target_found:
                best_dist = sys.maxsize

                for key, value in target_ind_dists.items():
                    if key < best_dist:
                        best_dist = key

                # Debug Purpose Only
                # target_ind_dists[best_dist][0].show()

                draw = ImageDraw.Draw(image_orig)
                draw.rectangle([target_ind_dists[best_dist][3], target_ind_dists[best_dist][1],
                                target_ind_dists[best_dist][4], target_ind_dists[best_dist][2]],
                               outline=(255, 255, 0, 255),
                               width=10)
                image_orig.show()

                # Clear variables
                del draw
                del target_ind_dists
    elif version == 2:
        return
    elif version == 3:
        global processed_images
        area_measure_avg_per_class = [0.0] * len(classes)
        total_any_object_found_image_count_per_class = [0.0] * len(classes)
        total_any_object_found_image_count_per_class_dict = dict(zip(classes, [0.0] * len(classes)))
        total_object_count_per_class = [0.0] * len(classes)
        total_multiple_image_found_per_class = [0.0] * len(classes)
        saved_target_image_curr_index = 1

        for class_index in range(len(classes)):
            if test_only and class_index <= 23:
                continue
            if str(class_index + 1) not in process_classes.split(','):
                continue

            area_measure_avg_single_class = [0]
            total_any_object_found_image_count_single_class = [0]
            total_object_count_single_class = [0]
            area_measure_list = dict((a, []) for a in classes)
            total_multiple_image_found_single_class = [0]
            total_multiple_image_found = 0

            # Target Name
            to_be_found_class_name = classes[class_index]
            spaced_to_be_found_class_name = classes[class_index].replace('+', ' ')

            folder_dir = root_path + 'data/Stimuli/V3/GeneratedStimuliImages/' \
                                   + ('Single' if is_single else 'Multiple') \
                                   + '/' + spaced_to_be_found_class_name
            file_descriptor = os.path.join(folder_dir, '*.jpg')
            image_file_paths = glob(file_descriptor)

            print("Processing " + spaced_to_be_found_class_name)

            image_file_index = 1
            for image_file_path in image_file_paths:
                to_be_found_class_names = [classes[class_index]]
                multiple_found_reset = True

                image_name = image_file_path[image_file_path.rindex(spaced_to_be_found_class_name)
                                             + len(spaced_to_be_found_class_name) + 1:]

                if image_name in processed_images:
                    continue

                processed_images.append(image_name)

                splitted_image_name = image_name.split('_')
                image_count = int(splitted_image_name[1])
                target_size = int(splitted_image_name[2])

                # X as vertical and Y as horizontal
                gt_start_y = int(splitted_image_name[3])
                gt_end_y = gt_start_y + target_size
                gt_start_x = int(splitted_image_name[4][:splitted_image_name[4].rindex('.')])
                gt_end_x = gt_start_x + target_size

                gt_values = dict()
                gt_values[classes[class_index]] = [gt_start_x, gt_end_x, gt_start_y, gt_end_y]
                gt_images = dict()
                # gt_images[classes[class_index]] = image_file_path.replace('GeneratedStimuliImages',
                #                                                          'GeneratedGroundTruth')

                if not is_single:
                    base_folder_dir = root_path + 'data/Stimuli/V3/GeneratedStimuliImages/Multiple/'

                    dup_index = dict()
                    dup_index[to_be_found_class_name] = 1
                    for class_name in classes:
                        spaced_class_name = class_name.replace('+', ' ')
                        search_folder_dir = base_folder_dir + spaced_class_name + '/'
                        candidate_file_path = os.path.join(search_folder_dir,
                                                           image_name.replace('_' + str(gt_start_y) + '_', '_')
                                                           .replace('_' + str(gt_start_x) + '.', '.').replace('__', '')
                                                           .replace('.jpg', '*.jpg'))
                        if (len(glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                 str(image_count + 1).zfill(3)))) > 0
                                or len(glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                        str(image_count - 1).zfill(3)))) > 0
                                or len(glob(candidate_file_path)) > 0):
                            spaced_to_be_found_class_name_alt = class_name.replace('+', ' ')
                            image_file_paths_alt = glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                                    str(image_count + 1).zfill(3))) \
                                if len(glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                        str(image_count + 1).zfill(3)))) > 0 \
                                else \
                                (glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                  str(image_count - 1).zfill(3)))
                                 if len(glob(candidate_file_path.replace(str(image_count).zfill(3),
                                                                         str(image_count - 1).zfill(3)))) > 0
                                 else glob(candidate_file_path))

                            for image_file_path_alt in image_file_paths_alt:
                                image_name_alt = image_file_path_alt[
                                                 image_file_path_alt.rindex(spaced_to_be_found_class_name_alt)
                                                 + len(spaced_to_be_found_class_name_alt) + 1:]

                                if image_name_alt == image_name:
                                    continue

                                splitted_image_name_alt = image_name_alt.split('_')
                                # target_size is same in also images that contain multiple targets
                                target_size = int(splitted_image_name_alt[2])
                                gt_start_y = int(splitted_image_name_alt[3])
                                gt_end_y = gt_start_y + target_size
                                gt_start_x = int(splitted_image_name_alt[4][:splitted_image_name_alt[4].rindex('.')])
                                gt_end_x = gt_start_x + target_size

                                if class_name not in gt_values:
                                    gt_values[class_name] = [gt_start_x, gt_end_x, gt_start_y, gt_end_y]
                                    # gt_images[class_name] = image_file_path.replace(
                                    #     'GeneratedStimuliImages',
                                    #     'GeneratedGroundTruth')
                                else:
                                    if class_name not in dup_index:
                                        dup_index[class_name] = 1

                                    gt_values[class_name + '#' + str(dup_index[class_name])] = \
                                        [gt_start_x, gt_end_x, gt_start_y, gt_end_y]
                                    # gt_images[class_name + '#' + str(dup_index[class_name])] = image_file_path
                                    #                                                            .replace(
                                    #    'GeneratedStimuliImages',
                                    #    'GeneratedGroundTruth')
                                    dup_index[class_name] = dup_index[class_name] + 1

                                to_be_found_class_names.append(class_name)
                                processed_images.append(image_name_alt)

                    if len(to_be_found_class_names) < 2:
                        print('Skipped a Target in Multiple Mode')

                image_orig = Image.open(image_file_path) \
                    .convert('RGBA')
                image = np.array(image_orig)

                at_least_one_target_found = False
                target_ind_dists = dict()
                target_views = [224, 196, 168, 140, 112]
                target_view_points = [round(1 - round(abs(224 - target_size) / 224, 2), 2),
                                      round(1 - round(abs(196 - target_size) / 196, 2), 2),
                                      round(1 - round(abs(168 - target_size) / 168, 2), 2),
                                      round(1 - round(abs(140 - target_size) / 140, 2), 2),
                                      round(1 - round(abs(112 - target_size) / 112, 2), 2)]

                dup_index = dict(zip(to_be_found_class_names, [1] * len(to_be_found_class_names)))
                for view_index in range(len(target_views)):
                    target_view = target_views[view_index]
                    if target_view > stimuli_width or target_view > stimuli_height:
                        continue

                    if precision_mode:
                        padding_view = target_view // 2
                    else:
                        padding_view = target_view
                    start_x = 0
                    end_x = target_view
                    start_y = 0
                    end_y = target_view

                    while True:
                        cropped_image_orig = image[start_x:end_x, start_y:end_y, :]
                        test_process_steps = transforms.Compose([
                            transforms.Resize((224, 224)),
                            transforms.ToTensor()
                        ])
                        cropped_image_orig = Image.fromarray(cropped_image_orig, 'RGBA')
                        cropped_image = test_process_steps(cropped_image_orig)
                        cropped_image = cropped_image.to(device)
                        cropped_image = cropped_image.reshape([1, 4, 224, 224])

                        outputs = loaded_model(cropped_image)
                        sigmoid_outputs = torch.sigmoid(outputs)
                        pred_labels = sigmoid_outputs

                        if 0 <= class_index <= 23:
                            mode = 'train'
                        else:
                            mode = 'test'

                        if mode == 'train2' or mode == 'test2':
                            temp = pred_labels[:, :]
                            temp[temp >= 0.5] = 1
                            temp[temp < 0.5] = 0

                        best_dist, best_index = label_to_class(pred_labels, predicate_binary_mat, classes,
                                                               train_classes,
                                                               mode)
                        best_dist = best_dist * target_view_points[view_index]
                        curr_pred_class = classes[best_index]

                        if curr_pred_class in to_be_found_class_names:
                            at_least_one_target_found = True
                            if curr_pred_class in target_ind_dists:
                                if str(curr_pred_class + '#1') in gt_values:
                                    if dup_index[curr_pred_class] < \
                                            len([i for i in list(gt_values.keys()) if i.startswith(curr_pred_class)]):
                                        target_ind_dists[curr_pred_class + '#' + str(dup_index)] = \
                                            [best_dist, start_x, end_x, start_y, end_y]
                                        dup_index[curr_pred_class] = dup_index[curr_pred_class] + 1
                                    else:
                                        min_dist = -sys.maxsize - 1
                                        captured_key = ''
                                        captured_dist = -sys.maxsize - 1
                                        for key, value in target_ind_dists.items():
                                            if key.startswith(curr_pred_class) and value[0] > min_dist:
                                                captured_key = key
                                                captured_dist = value[0]
                                                min_dist = value[0]

                                        if captured_key != '':
                                            if best_dist < captured_dist:
                                                target_ind_dists[captured_key] = \
                                                    [best_dist, start_x, end_x, start_y, end_y]
                                        else:
                                            target_ind_dists[str(curr_pred_class + '#1')] = \
                                                [best_dist, start_x, end_x, start_y, end_y]

                                elif best_dist < target_ind_dists[curr_pred_class][0]:
                                    target_ind_dists[curr_pred_class] = [best_dist, start_x, end_x, start_y, end_y]
                            else:
                                target_ind_dists[curr_pred_class] = [best_dist, start_x, end_x, start_y, end_y]

                        start_y = start_y + padding_view
                        end_y = start_y + target_view
                        if start_y > stimuli_width:
                            start_y = 0
                            end_y = target_view
                            start_x = start_x + padding_view
                            end_x = start_x + target_view

                        if end_y > stimuli_width:
                            end_y = stimuli_width

                        if end_x > stimuli_height:
                            end_x = stimuli_height

                        if start_x > stimuli_height:
                            break

                        if end_y - start_y > (end_x - start_x) * 1.25:
                            break

                        if end_x - start_x > (end_y - start_y) * 1.25:
                            start_y = 0
                            end_y = target_view
                            start_x = start_x + padding_view
                            end_x = start_x + target_view

                            if end_x > stimuli_height:
                                end_x = stimuli_height

                            if start_x > stimuli_height:
                                break

                if at_least_one_target_found:
                    multiple_found_check = False

                    for key, value in target_ind_dists.items():
                        trimmed_key = key
                        if '#' in key:
                            trimmed_key = key[:key.index('#')]

                        target_start_x = value[1]
                        target_end_x = value[2]
                        target_start_y = value[3]
                        target_end_y = value[4]

                        target_x = range(target_start_x, target_end_x)
                        target_y = range(target_start_y, target_end_y)

                        # Groundtruth Image
                        # gt_image_file_path = gt_images[key]
                        # black_white_gt_image = Image.open(gt_image_file_path).convert('1')
                        # black_white_gt_image = np.array(black_white_gt_image)

                        # Compare the found target region with groundtruth region
                        gt_start_x = gt_values[key][0]
                        gt_end_x = gt_values[key][1]
                        gt_start_y = gt_values[key][2]
                        gt_end_y = gt_values[key][3]

                        gt_x = range(gt_start_x, gt_end_x)
                        gt_y = range(gt_start_y, gt_end_y)

                        intersection_x = set(target_x).intersection(gt_x)
                        intersection_y = set(target_y).intersection(gt_y)

                        temp_intersection_x = set()
                        temp_intersection_y = set()
                        overlap_x = 0
                        overlap_y = 0
                        if (target_start_x < gt_start_x < target_end_x) or (
                                target_start_x < gt_end_x < target_end_x):
                            temp_intersection_x = set(target_x).union(gt_x)
                            overlap_x = len(intersection_x) + ((len(temp_intersection_x) - len(intersection_x)) / 8)
                        else:
                            overlap_x = len(intersection_x)
                        if (target_start_y < gt_start_y < target_end_y) or (
                                target_start_y < gt_end_y < target_end_y):
                            temp_intersection_y = set(target_y).union(gt_y)
                            overlap_y = len(intersection_y) + ((len(temp_intersection_y) - len(intersection_y)) / 8)
                        else:
                            overlap_y = len(intersection_y)

                        total_area = (gt_end_x - gt_start_x) * (gt_end_y - gt_start_y)
                        total_overlapped_area = overlap_x * overlap_y

                        # total_overlapped_area / total_area
                        area_measure = (round(round(1
                                                    - round(abs(total_overlapped_area - total_area) / total_area, 2)
                                                    , 2)
                                              * 100, 2))

                        if area_measure > 0.0:
                            print('Classes[{}/{}] Images[{}/{}] To Be Found Class Name(s): {} || POTENTIAL '
                                  'TARGET(S) ARE FOUND || Overlapping Area Percentage: {} '
                                  '|| GT X(H): {} || GT Y(W): {} '
                                  '|| PT X(H): {} || PT Y(W): {}'
                                  .format(class_index + 1,
                                          len(classes),
                                          image_file_index,
                                          len(image_file_paths),
                                          ','.join(to_be_found_class_names),
                                          area_measure,
                                          str(gt_start_x) + '-' + str(gt_end_x),
                                          str(gt_start_y) + '-' + str(gt_end_y),
                                          str(target_start_x) + '-' + str(target_end_x),
                                          str(target_start_y) + '-' + str(target_end_y)))

                            if not is_single and multiple_found_check and multiple_found_reset:
                                total_multiple_image_found = total_multiple_image_found + 1
                                multiple_found_reset = False

                            total_any_object_found_image_count_per_class_dict[trimmed_key] = \
                                total_any_object_found_image_count_per_class_dict[trimmed_key] + 1
                            area_measure_list[trimmed_key].append(area_measure)
                            multiple_found_check = True

                            # Debug Purpose Only
                            if debug_mode and saved_target_image_curr_index <= max_save_target_image_count:
                                if not path.exists(root_path + 'output/targets/'):
                                    os.mkdir(root_path + 'output/targets/')

                                draw = ImageDraw.Draw(image_orig)
                                draw.rectangle([target_start_y, target_start_x,
                                                target_end_y, target_end_x],
                                               outline=(255, 255, 0, 255),
                                               width=10)
                                image_orig.save(root_path + 'output/targets/test_detected_target_figure#'
                                                + str(saved_target_image_curr_index) + '_' + str(area_measure) + '.png')
                                saved_target_image_curr_index = saved_target_image_curr_index + 1
                                # Clear variable
                                del draw
                        else:
                            dup_index = 1
                            while trimmed_key + '#' + str(dup_index) in gt_values:
                                if trimmed_key + '#' + str(dup_index) == key:
                                    continue

                                gt_start_x = gt_values[trimmed_key + '#' + str(dup_index)][0]
                                gt_end_x = gt_values[trimmed_key + '#' + str(dup_index)][1]
                                gt_start_y = gt_values[trimmed_key + '#' + str(dup_index)][2]
                                gt_end_y = gt_values[trimmed_key + '#' + str(dup_index)][3]

                                gt_x = range(gt_start_x, gt_end_x)
                                gt_y = range(gt_start_y, gt_end_y)

                                intersection_x = set(target_x).intersection(gt_x)
                                intersection_y = set(target_y).intersection(gt_y)

                                temp_intersection_x = set()
                                temp_intersection_y = set()
                                overlap_x = 0
                                overlap_y = 0
                                if (target_start_x < gt_start_x < target_end_x) or (
                                        target_start_x < gt_end_x < target_end_x):
                                    temp_intersection_x = set(target_x).union(gt_x)
                                    overlap_x = len(intersection_x) + (
                                                (len(temp_intersection_x) - len(intersection_x)) / 8)
                                else:
                                    overlap_x = len(intersection_x)
                                if (target_start_y < gt_start_y < target_end_y) or (
                                        target_start_y < gt_end_y < target_end_y):
                                    temp_intersection_y = set(target_y).union(gt_y)
                                    overlap_y = len(intersection_y) + (
                                                (len(temp_intersection_y) - len(intersection_y)) / 8)
                                else:
                                    overlap_y = len(intersection_y)

                                total_area = (gt_end_x - gt_start_x) * (gt_end_y - gt_start_y)
                                total_overlapped_area = overlap_x * overlap_y

                                # total_overlapped_area / total_area
                                area_measure = (round(round(1
                                                            - round(abs(total_overlapped_area
                                                                        - total_area) / total_area, 2)
                                                            , 2)
                                                      * 100, 2))

                                if area_measure > 0.0:
                                    print('Classes[{}/{}] Images[{}/{}] To Be Found Class Name(s): {} || POTENTIAL '
                                          'TARGET(S) ARE FOUND || Overlapping Area Percentage: {} '
                                          '|| GT X(H): {} || GT Y(W): {} '
                                          '|| PT X(H): {} || PT Y(W): {}'
                                          .format(class_index + 1,
                                                  len(classes),
                                                  image_file_index,
                                                  len(image_file_paths),
                                                  ','.join(to_be_found_class_names),
                                                  area_measure,
                                                  str(gt_start_x) + '-' + str(gt_end_x),
                                                  str(gt_start_y) + '-' + str(gt_end_y),
                                                  str(target_start_x) + '-' + str(target_end_x),
                                                  str(target_start_y) + '-' + str(target_end_y)))

                                    if not is_single and multiple_found_check and multiple_found_reset:
                                        total_multiple_image_found = total_multiple_image_found + 1
                                        multiple_found_reset = False

                                    total_any_object_found_image_count_per_class_dict[trimmed_key] = \
                                        total_any_object_found_image_count_per_class_dict[trimmed_key] + 1
                                    area_measure_list[trimmed_key].append(area_measure)
                                    multiple_found_check = True

                                    # Debug Purpose Only
                                    if debug_mode and saved_target_image_curr_index <= max_save_target_image_count:
                                        if not path.exists(root_path + 'output/targets/'):
                                            os.mkdir(root_path + 'output/targets/')

                                        draw = ImageDraw.Draw(image_orig)
                                        draw.rectangle([target_start_y, target_start_x,
                                                        target_end_y, target_end_x],
                                                       outline=(255, 255, 0, 255),
                                                       width=10)
                                        image_orig.save(root_path + 'output/targets/test_detected_target_figure#'
                                                        + str(saved_target_image_curr_index) + '_'
                                                        + str(area_measure) + '.png')
                                        saved_target_image_curr_index = saved_target_image_curr_index + 1
                                        # Clear variable
                                        del draw

                                dup_index = dup_index + 1

                    # Clear variables
                    del target_ind_dists
                else:
                    print('Classes[{}/{}] Images[{}/{}] To Be Found Class Name(s): {} || NO TARGET IS FOUND'
                          .format(class_index + 1,
                                  len(classes),
                                  image_file_index,
                                  len(image_file_paths),
                                  ','.join(to_be_found_class_names)))

                image_file_index = image_file_index + 1

            if len(area_measure_list[to_be_found_class_name]) > 0:
                area_measure_avg_per_class[class_index] = (sum(area_measure_list[to_be_found_class_name])
                                                           / len(area_measure_list[to_be_found_class_name]))
            total_any_object_found_image_count_per_class[class_index] = \
                total_any_object_found_image_count_per_class_dict[to_be_found_class_name]
            total_object_count_per_class[class_index] = len(image_file_paths)
            total_multiple_image_found_per_class[class_index] = total_multiple_image_found

            if len(area_measure_list[to_be_found_class_name]) > 0:
                area_measure_avg_single_class[0] = (sum(area_measure_list[to_be_found_class_name])
                                                    / len(area_measure_list[to_be_found_class_name]))
            total_any_object_found_image_count_single_class[0] = \
                total_any_object_found_image_count_per_class_dict[to_be_found_class_name]
            total_object_count_single_class[0] = len(image_file_paths)
            total_multiple_image_found_single_class[0] = total_multiple_image_found

            plot_precision_bar(root_path, [to_be_found_class_name], area_measure_avg_single_class, True,
                               is_single, True, to_be_found_class_name, not test_only)
            plot_rate_bar(root_path, [to_be_found_class_name], total_any_object_found_image_count_single_class,
                          total_object_count_single_class, True, is_single, True, to_be_found_class_name, not test_only)
            if not is_single:
                plot_multiple_rate_bar(root_path, [to_be_found_class_name], total_multiple_image_found_single_class,
                                       total_object_count_single_class, True, True,
                                       to_be_found_class_name, not test_only)

        plot_precision_bar(root_path, classes, area_measure_avg_per_class, True, is_single, False, None, not test_only)
        plot_rate_bar(root_path, classes, total_any_object_found_image_count_per_class,
                      total_object_count_per_class, True, is_single, False, None, not test_only)
        if not is_single:
            plot_multiple_rate_bar(root_path, classes, total_multiple_image_found_per_class,
                                   total_object_count_per_class, True, False, None, not test_only)


def plot_precision_bar(root_path, x_labels, y_labels, is_vertical, is_single, single_class, class_name, is_mixed):
    y_labels_mod = y_labels.copy()
    # if len(y_labels_mod) < len(x_labels):
    #     for i in range(len(x_labels) - len(y_labels_mod)):
    #         y_labels_mod.append(0)

    indexes = np.arange(len(x_labels))

    if single_class:
        pyplot.figure(figsize=(5, 10))
    else:
        pyplot.figure(figsize=(10, 15))

    if is_vertical:
        pyplot.bar(indexes, y_labels_mod)
    else:
        pyplot.barh(indexes, y_labels_mod)

    pyplot.xlabel('Classes', fontsize=10)
    pyplot.ylabel('Avg Detected Object Boundaries (Percentage)', fontsize=10)
    if single_class:
        pyplot.xticks(indexes, x_labels, fontsize=10, rotation=0)
    else:
        pyplot.xticks(indexes, x_labels, fontsize=5, rotation=90)
    pyplot.suptitle('Class / Target Found Success \n (in terms of overlapped area with groundtruth)', fontsize=10)

    if not path.exists(root_path + 'output/figures/'):
        os.mkdir(root_path + 'output/figures/')

    pyplot.savefig(root_path + 'output/figures/avg_object_boundaries_' + ('single' if is_single else 'multiple')
                   + ('_' + class_name if single_class else ('' if is_mixed else '_test')) + '.png', dpi=500)
    pyplot.close()


def plot_rate_bar(root_path, x_labels, y_labels_1, y_labels_2, is_vertical, is_single, single_class, class_name,
                  is_mixed):
    # zip_obj = zip(y_labels_1, y_labels_2)
    # y_labels_1_mod = y_labels_1.copy()
    # y_labels_2_mod = []

    # for y_labels_1_i, y_labels_2_i in zip_obj:
    #    y_labels_2_mod.append(y_labels_2_i - y_labels_1_i)

    # if len(y_labels_2_mod) < len(x_labels):
    #     for i in range(len(x_labels) - len(y_labels_2_mod)):
    #         y_labels_1_mod.append(0)
    #         y_labels_2_mod.append(0)

    indexes = np.arange(len(x_labels))

    if single_class:
        pyplot.figure(figsize=(5, 10))
    else:
        pyplot.figure(figsize=(10, 15))

    if is_vertical:
        pyplot.bar(indexes, y_labels_2, label='Total Available Targets', color='#FF7F0e')
        pyplot.bar(indexes, y_labels_1, label='Total Found Targets', width=0.7, color='#1F77B4')
    else:
        pyplot.barh(indexes, y_labels_2, label='Total Available Targets', color='#FF7F0e')
        pyplot.barh(indexes, y_labels_1, label='Total Found Targets', width=0.7, color='#1F77B4')

    pyplot.xlabel('Classes', fontsize=10)
    pyplot.ylabel('Total Targets Found Per Class (comparison with total to be found targets)', fontsize=10)
    if single_class:
        pyplot.xticks(indexes, x_labels, fontsize=10, rotation=0)
    else:
        pyplot.xticks(indexes, x_labels, fontsize=5, rotation=90)
    pyplot.suptitle('Total Targets Found Per Class', fontsize=10)
    pyplot.legend(loc="upper right")

    if not path.exists(root_path + 'output/figures/'):
        os.mkdir(root_path + 'output/figures/')

    pyplot.savefig(root_path + 'output/figures/total_target_found_per_class_' + ('single' if is_single else 'multiple')
                   + ('_' + class_name if single_class else ('' if is_mixed else '_test')) + '.png', dpi=500)
    pyplot.close()


def plot_multiple_rate_bar(root_path, x_labels, y_labels_1, y_labels_2, is_vertical, single_class, class_name,
                           is_mixed):
    # zip_obj = zip(y_labels_1, y_labels_2)
    # y_labels_1_mod = y_labels_1.copy()
    # y_labels_2_mod = []

    # for y_labels_1_i, y_labels_2_i in zip_obj:
    #    y_labels_2_mod.append(y_labels_2_i - y_labels_1_i)

    # if len(y_labels_2_mod) < len(x_labels):
    #     for i in range(len(x_labels) - len(y_labels_2_mod)):
    #         y_labels_1_mod.append(0)
    #         y_labels_2_mod.append(0)

    indexes = np.arange(len(x_labels))

    pyplot.figure(figsize=(10, 15))

    if is_vertical:
        pyplot.bar(indexes, y_labels_2, label='Total Target Search Images', color='#FF7F0e')
        pyplot.bar(indexes, y_labels_1, label='Total Found Multiple Target Images', width=0.7, color='#1F77B4')
    else:
        pyplot.barh(indexes, y_labels_2, label='Total Target Search Images', color='#FF7F0e')
        pyplot.barh(indexes, y_labels_1, label='Total Found Multiple Target Images', width=0.7, color='#1F77B4')

    pyplot.xlabel('Classes', fontsize=10)
    pyplot.ylabel('Total Multiple Targets Found Per Class (comparison with total target search images)', fontsize=10)
    pyplot.xticks(indexes, x_labels, fontsize=5, rotation=90)
    pyplot.suptitle('Total Multiple Targets Found Per Class', fontsize=10)
    pyplot.legend(loc="upper right")

    if not path.exists(root_path + 'output/figures/'):
        os.mkdir(root_path + 'output/figures/')

    pyplot.savefig(root_path + 'output/figures/total_multiple_targets_found_per_class_multiple'
                   + ('_' + class_name if single_class else ('' if is_mixed else '_test')) + '.png', dpi=500)
    pyplot.close()


def main():
    global processed_images
    print("[" + str(datetime.now()) + "] Process started")

    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_35_ones/'
    elif plt == 'Linux':
        root_path = '/home/ubuntu/bil635-zeroshot_visualsearch_v02/targets_with_attributes' \
                    '/first_35_ones/'
    else:
        sys.exit(0)

    parser = argparse.ArgumentParser()
    parser.add_argument('--test_only', '-t', type=arg2bool, nargs='?', const=True, default=False)
    parser.add_argument('--precision_mode', '-p', type=arg2bool, nargs='?', const=True, default=False)
    parser.add_argument('--process_classes', '-c', type=str, default='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,'
                                                                     '20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35')
    parser.add_argument('--debug_mode', '-d', type=arg2bool, nargs='?', const=True, default=False)
    parser.add_argument('--max_target_image', '-i', type=int, default=50)
    parser.add_argument('--mode', '-m', type=str, default='all')
    args = parser.parse_args()
    args = vars(args)
    test_only = args['test_only']
    precision_mode = args['precision_mode']
    process_classes = args['process_classes']
    mode = args['mode']
    debug_mode = args['debug_mode']
    max_save_target_image_count = args['max_target_image']

    print('Test Only: {}'.format(test_only))
    print('Precision Mode: {}'.format(precision_mode))
    if test_only:
        process_classes = '25,26,27,28,29,30,31,32,33,34,35'
    print('Process Classes: {}'.format(process_classes))
    print('Mode: {}'.format(mode))
    print('Debug Mode (Saving captured target images): {}'.format(debug_mode))
    print('Max Target Image Count to Save: {}'.format(max_save_target_image_count))

    predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))
    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))
    test_classes = np.array(np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str'))

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    stimuli_width = 1280
    stimuli_height = 1024
    target_view = 224
    padding_view = 112

    is_parallel = torch.cuda.device_count() > 1
    loaded_model = build_model(predicate_binary_mat.shape[1], False, is_parallel, device)
    loaded_model = load_model(loaded_model, root_path + 'output/models/model.bin', device, is_parallel)

    loaded_model.train(False)
    loaded_model.eval()

    if mode == 'all' or mode == 'single':
        print("Processing single images..")
        process_images(classes, train_classes, predicate_binary_mat, root_path, True, device, loaded_model,
                       stimuli_width, stimuli_height, padding_view, 3, test_only, precision_mode, process_classes,
                       debug_mode, max_save_target_image_count)

    if mode == 'all' or mode == 'multiple':
        print("Processing multiple images..")
        process_images(classes, train_classes, predicate_binary_mat, root_path, False, device, loaded_model,
                       stimuli_width, stimuli_height, padding_view, 3, test_only, precision_mode, process_classes,
                       debug_mode, max_save_target_image_count)

    print("[" + str(datetime.now()) + "] Process finished. Scanned total of "
          + str(len(processed_images)) + " stimuli images..")
    sys.exit(0)


if __name__ == '__main__':
    main()
