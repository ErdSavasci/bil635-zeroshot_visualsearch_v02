import numpy as np
import os
import torchvision.transforms as transforms
import torchvision.transforms.functional as functional
import random
from glob import glob
from PIL import Image
from torch.utils import data


class TargetDataset(data.dataset.Dataset):
    def __init__(self, root_path, classes_file, transform, is_single_mode, manual_transform_mode):
        predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))
        self.predicate_binary_mat = predicate_binary_mat
        self.transform = transform
        self.manual_transform_mode = manual_transform_mode

        class_to_index = dict()
        total_classes = 0
        # Build dictionary of indices to classes
        with open(root_path + 'data/{}'.format(classes_file)) as f:
            for line in f:
                class_index = line.split('\t')[0].strip()
                class_name = line.split('\t')[1].strip()
                class_to_index[class_name] = int(class_index) - 1
                total_classes = total_classes + 1
        self.class_to_index = class_to_index

        img_names = []
        img_index = []
        total_image_in_dataset = 0
        with open(root_path + 'data/{}'.format(classes_file)) as f:
            for line in f:
                class_name = line.split('\t')[1].strip()
                class_index = class_to_index[class_name]
                class_index_str = str(class_index + 1).zfill(3)

                trimmed_class_name = class_name.replace('+', '')
                spaced_class_name = class_name.replace('+', ' ')

                if not is_single_mode:
                    folder_dir = os.path.join(root_path + 'data/MyDataset/' + spaced_class_name)
                    file_descriptor = os.path.join(folder_dir, trimmed_class_name + '*.png')
                else:
                    folder_dir = os.path.join(root_path + 'data/PNGImages')
                    file_descriptor = os.path.join(folder_dir, 't' + class_index_str + '*.png')

                files = glob(file_descriptor)

                print(str(len(files)) + ' files found for class ' + class_name)
                total_image_in_dataset += len(files)

                for file_name in files:
                    img_names.append(file_name)
                    img_index.append(class_index)

        mode = 'TRAINING' if 'train' in classes_file else 'TEST'
        print('[' + mode + ']' + ' Total classes: ' + str(total_classes) + ' | Total dataset images: '
              + str(total_image_in_dataset))

        self.img_names = img_names
        self.img_index = img_index

    def alterimage(self, im, index):
        modified = False
        if (index == -1 and random.random() > 0.5) or index == 0:
            im = functional.adjust_brightness(im, 0.5)
            im = functional.adjust_contrast(im, 0.5)
            modified = True
        if (index == -1 and random.random() > 0.5) or index == 1:
            t, l, h, w = transforms.RandomCrop.get_params(im, (224, 224))
            im = functional.crop(im, t, l, h, w)
            modified = True
        if (index == -1 and random.random() > 0.5) or index == 2:
            angle = transforms.RandomRotation.get_params([-15, 15])
            im = functional.rotate(im, angle)
            modified = True
        if (index == -1 and random.random() > 0.5) or index == 3:
            im = functional.hflip(im)
            modified = True
        if (index == -1 and random.random() > 0.5) or index == 4:
            im = functional.vflip(im)
            modified = True
        return im, modified

    def __getitem__(self, index):
        im = Image.open(self.img_names[index])
        if im.getbands()[0] == 'L':
            im = im.convert('RGB')
        if not self.manual_transform_mode and self.transform:
            im = self.transform(im)
        else:
            im = functional.resize(im, (256, 256))
            im, modified = self.alterimage(im, -1)

            if not modified:
                im, modified = self.alterimage(im, random.randint(0, 4))

            im = functional.resize(im, (224, 224))
            im = transforms.functional.to_tensor(im)
        if im.shape != (3, 224, 224) and im.shape != (4, 224, 224):
            print('Wrong Shaped File: ' + self.img_names[index])

        im_index = self.img_index[index]
        im_predicate = self.predicate_binary_mat[im_index, :]
        return im, im_predicate, self.img_names[index], im_index

    def __len__(self):
        return len(self.img_names)
