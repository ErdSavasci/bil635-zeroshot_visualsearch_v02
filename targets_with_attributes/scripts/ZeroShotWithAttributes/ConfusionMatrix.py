from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as pyplot
import numpy as np
import os
import platform
import sys

plt = platform.system()


def draw_confusion_matrix(root_path):
    labels = np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str').tolist()
    labels_dict = {}
    only_class_labels = []

    index = 0
    for label in labels:
        labels_dict[label[1]] = index
        only_class_labels.append(label[1])
        index += 1

    with open(root_path + 'output/predictions_test.txt', 'r') as f:
        predicted_classes = []
        true_classes = []
        for line in f:
            true_class, pred_class = line.strip().split('###')
            true_class = true_class.split('/')[0]
            if ' ' in true_class:
                true_class = true_class.replace(' ', '+')
            true_classes.append(true_class)
            predicted_classes.append(pred_class)

    confusion_mat = np.array(confusion_matrix(true_classes, predicted_classes, only_class_labels))
    confusion_mat = confusion_mat / np.sum(confusion_mat, axis=1)
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(confusion_mat)
    fig.colorbar(cax)
    ax.set_xticks(range(len(labels)))
    ax.set_xticklabels(labels, rotation=45, ha='left', rotation_mode='anchor')
    ax.set_yticks(range(len(labels)))
    ax.set_yticklabels(labels)
    pyplot.tight_layout()
    pyplot.xlabel('Predicted', fontweight='bold')
    pyplot.ylabel('True', fontweight='bold')

    if not os.path.exists(root_path + 'output/figures'):
        os.mkdir(root_path + 'output/figures')
    pyplot.savefig(root_path + 'output/figures/confusion_matrix.png', dpi=500)

    sys.exit(0)


if __name__ == '__main__':
    _root_path = ''

    if plt == 'Windows':
        _root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_35_ones/'
    elif plt == 'Linux':
        _root_path = '/home/ubuntu/bil635-zeroshot_visualsearch_v02/targets_with_attributes' \
                    '/first_35_ones/'
    else:
        sys.exit(0)

    draw_confusion_matrix(_root_path)
